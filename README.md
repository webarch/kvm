# Ansible KVM Role

An Ansible role to install and enable KVM (Kernel-based Virtual Machine) on
Debian and Ubuntu based on [Installing KVM on Debian
10](https://linuxhint.com/install_kvm_debian_10/).

Add youself to the `libvirt` group:

```bash
sudo usermod --append --groups libvirt $(whoami)
```

See also the [localhost role](https://git.coop/webarch/localhost).
